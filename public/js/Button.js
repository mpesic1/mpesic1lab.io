

class Button extends PIXI.Sprite {

    constructor(text, onClick) {
        let btnTexture = PIXI.Loader.shared.resources["button"].texture;
        let btnHoverTexture = PIXI.Loader.shared.resources["buttonHover"].texture;
        
        super(btnTexture);

        this.anchor.set(0.5);
        this.interactive = true;
        this.buttonMode = true;

        let btnTxt = new PIXI.Text(text,
            {
                fontFamily : 'MotionControl',
                fontSize: 32 / this.scale.y, 
                fill : 0x333333, 
                align : 'center'
            }
        );
    
        btnTxt.anchor.set(0.5);
        btnTxt.scale.x = 2;

        this.addChild(btnTxt);    
        this.scale.x = 0.5;
    
        this  
            .on('pointerover', () => this.texture = btnHoverTexture)
            .on('pointerout', () => this.texture = btnTexture)
            .on("pointerup", onClick);
    }

}


export default Button;