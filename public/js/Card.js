

class Card extends PIXI.Sprite {

    static suits = ['hearts', 'clubs', 'diamonds', 'spades'];
    static ranks = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'jack': 10, 'queen': 10, 'king': 10, 'ace': 11};

    constructor(suit, rank, value) {
        super(PIXI.Loader.shared.resources[rank + "_of_" + suit].texture);
        this.textureFaceUp = PIXI.Loader.shared.resources[rank + "_of_" + suit].texture;
        this.textureFaceDown = PIXI.Loader.shared.resources["cardBack"].texture;
        this.anchor.set(0.5);
        this.scale.set(0.6);
        this.suit = suit;
        this.rank = rank;
        this.value = value;
        this.fileName = rank + "_of_" + suit;
        this.isFaceDown = false;
    }

    setFaceDown() {
        this.isFaceDown = true;
        this.texture = this.textureFaceDown;
    }

    setFaceUp() {
        this.isFaceDown = false;
        this.texture = this.textureFaceUp;
    }

}



export default Card;