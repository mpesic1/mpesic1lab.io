

class Coin extends PIXI.Sprite {

    static COIN_SIZE = 100;

    constructor(value) {

        let texture = PIXI.Loader.shared.resources["coin"].texture;

        super(texture);
        this.value = value;

        this.anchor.set(0.5, 0.5);
        this.interactive = true;
        this.buttonMode = true;
        this.width = Coin.COIN_SIZE;
        this.height = Coin.COIN_SIZE;
    
        // register click only in the circle, not the whole sprite
        this.hitArea = new PIXI.Circle(0, 0, 0.5 * Coin.COIN_SIZE / this.scale.x);
    
        let valueTxt = new PIXI.Text(value,
            {
                fontFamily : 'MotionControl',
                fontSize: 32 / this.scale.x, 
                fill : 0x333333, 
                align : 'center'
            }
        );
    
        valueTxt.anchor.set(0.5, 0.5);
        this.addChild(valueTxt);    
        
    }

}


export default Coin;