import Card from './Card.js';
import Deck from './Deck.js';
import Coin from './Coin.js';
import Button from './Button.js'



let type = "WebGL"

if(!PIXI.utils.isWebGLSupported()){
    type = "canvas"
}

// create a Pixi Application
let app = new PIXI.Application({width: 256, height: 256});
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoDensity = true;
app.renderer.resize(window.innerWidth, window.innerHeight);
app.renderer.backgroundColor = 0x519c64;

document.getElementById("display").appendChild(app.view);

// declare vars
let deck;
let balance;
let bet;
let balanceText;
let statusText;
let coins;
let dealButton;
let betText;
let dealerCards;
let playerCards;

let standButton;
let hitButton;
let splitButton;
let doubleButton;
let waitForClickToReset;

let playerHandText;
let dealerHandText;

const COIN_VALUES = [1, 5, 10, 50, 100];


// temporary hack
let a = false;
window.addEventListener("click", () => {
    if(a) {
        waitForClickToReset = true;
        a = false; 
        return;
    }
    if(waitForClickToReset) {
        reset();
    }
});


// load textures
PIXI.Loader.shared.add("coin", "res/coins/coin-256.png");
PIXI.Loader.shared.add("button", "res/buttons/grey_button00.png");
PIXI.Loader.shared.add("buttonHover", "res/buttons/grey_button02.png");
PIXI.Loader.shared.add("cardBack", "res/svg-cards/card_back.svg");

Card.suits.forEach(suit => {
    for(var rank in Card.ranks) {
        let fileName = rank + "_of_" + suit;
        PIXI.Loader.shared.add(fileName, "res/svg-cards/" + fileName + ".svg");
    }
});

// call setup() after all resources are loaded
PIXI.Loader.shared.load(setup);


  // do this after loading textures
function setup() {
    deck = new Deck();
    deck.shuffle();
    balance = 1000;
    bet = 0;
    dealerCards = [];
    playerCards = [];

    // balance text
    balanceText = createPlainText();
    balanceText.text = "BALANCE: $" + balance;
    balanceText.position.set(10, 3);
    app.stage.addChild(balanceText);

    // bet text
    betText = createPlainText();
    betText.text = "BET: $" + bet;
    betText.position.set(10, 50);
    app.stage.addChild(betText);

    // status text
    statusText = createPlainText();
    statusText = new PIXI.Text("Place your bets...",
        {
            fontFamily : 'MotionControl',
            fontSize: 64,  
            fill : 0xdddddd,
            stroke : 0x333333, 
            strokeThickness: 4, 
            align : 'center',
            dropShadow: true,
            dropShadowBlur: 2,
        }
    );

    statusText.anchor.set(0.5);
    statusText.position.set(0.5 * app.renderer.width, 0.5 * app.renderer.height);
    app.stage.addChild(statusText);

    // playerHandText
    playerHandText = createPlainText("0");
    playerHandText.anchor.set(0.5);
    playerHandText.position.set(0.5 * app.renderer.width, app.renderer.height - 150);
    playerHandText.visible = false;
    app.stage.addChild(playerHandText);

    // dealerHandText
    dealerHandText = createPlainText("0");
    dealerHandText.anchor.set(0.5);
    dealerHandText.position.set(0.5 * app.renderer.width, 150);
    dealerHandText.visible = false;
    app.stage.addChild(dealerHandText);

    // betting coins
    initCoins();

    // deal button
    dealButton = new Button("DEAL", dealButtonPressed);
    dealButton.position.set(0.5 * app.renderer.width, 0.5 * app.renderer.height + 100);
    app.stage.addChild(dealButton);

    // hit button
    hitButton = new Button("HIT", hitButtonPressed);
    hitButton.position.set(0.5 * app.renderer.width - 150, 0.5 * app.renderer.height);
    hitButton.visible = false;
    app.stage.addChild(hitButton);

    // stand button
    standButton = new Button("STAND", standButtonPressed);
    standButton.position.set(0.5 * app.renderer.width + 150, 0.5 * app.renderer.height);
    standButton.visible = false;
    app.stage.addChild(standButton);

    // double button
    doubleButton = new Button("DOUBLE", doubleButtonPressed);
    doubleButton.position.set(0.5 * app.renderer.width, 0.5 * app.renderer.height + 50);
    doubleButton.visible = false;
    app.stage.addChild(doubleButton);

    // split button
    splitButton = new Button("SPLIT", splitButtonPressed);
    splitButton.position.set(0.5 * app.renderer.width, 0.5 * app.renderer.height);
    splitButton.visible = false;
    app.stage.addChild(splitButton);
}


function createPlainText(text = "") {
    return new PIXI.Text(text,
        {
            fontFamily : 'MotionControl',
            fontSize: 64, 
            fill : 0xeeeeee, 
            align : 'center'
        }
    );
}


function initCoins() {
    coins = [];
    for(let i = 0; i < COIN_VALUES.length; i++) {
        let coin = new Coin(COIN_VALUES[i]);
        coin.x = 0.5 * app.renderer.width + (i - 2) * 120; 
        coin.y = app.renderer.height - 100;
        coin.on("pointerup", () => {
            changeBet(coin.value);
            changeBalance(-coin.value);
            updateVisibleCoins();
        });
        coins.push(coin);
        app.stage.addChild(coin);
     }
}


function changeBet(value) {
    bet += value;
    betText.text = "BET: $" + bet;
}


function changeBalance(value) {
    balance += value;
    balanceText.text = "BALANCE: $" + balance;    
}


// show or hide the sprite 
function updateVisibleCoins() {
    for(let i = 0; i < coins.length; i++) {
        let currCoin = coins[i];

        if(balance < currCoin.value) {
            currCoin.visible = false;
        }
        else {
            currCoin.visible = true;
        }
    }
}


// --------------------- CLICK EVENTS ---------------------

function dealButtonPressed() {
    if(bet <= 0) return;

    playerHandText.visible = true;
    dealerHandText.visible = true;

    // hide betting ui
    statusText.visible = false;
    dealButton.visible = false;
    for(let i = 0; i < coins.length; i++) {
        coins[i].visible = false;
    }

    // deal first four cards
    for(let i = 0; i < 4; i++) {
        let currCard = deck.nextCard();
        if(i == 3) currCard.setFaceDown();
        i % 2 == 0 ? addPlayerCard(currCard) : addDealerCard(currCard);
    }

    // check for blackjack
    if( getHandValue(playerCards) === 21 ||  getHandValue(dealerCards) === 21) {
        dealerCards[1].setFaceUp();
        determineWinner();
        return;
    }

    // show hand ui
    standButton.visible = true;
    hitButton.visible = true;

    if(balance >= bet) {
        doubleButton.visible = true;
    }

    if(playerCards[0].value === playerCards[1].value) {
        splitButton.visible = true;
    }

}


function hitButtonPressed() {
    splitButton.visible = false;
    doubleButton.visible = false;

    let card = deck.nextCard();
    addPlayerCard(card);
    if(getHandValue(playerCards) > 21) {
        dealerCards[1].setFaceUp();
        determineWinner();
    }
    else if(getHandValue(playerCards) === 21) {
        standButtonPressed();
    }
}


function standButtonPressed() {    
    dealerCards[1].setFaceUp();

    while(getHandValue(dealerCards) < 17) {
        let card = deck.nextCard();
        addDealerCard(card);
    }
    
    determineWinner();
}


function doubleButtonPressed() {
    if(balance >= bet) {
        changeBalance(-bet);
        changeBet(bet);
        hitButtonPressed();
        standButtonPressed();
    }
}

function splitButtonPressed() {}




function updateHandValuesTexts() {

    playerHandText.text = getHandValue(playerCards);
    
    if(dealerCards.length > 0) {
        if(dealerCards[dealerCards.length -1].isFaceDown) {
            dealerHandText.text = dealerCards[0].value;
        }
        else {
            dealerHandText.text = getHandValue(dealerCards);
        }    
    }
    
}


function addPlayerCard(card) {
    playerCards.push(card);
    updateHandValuesTexts();
    for(let i = 0; i < playerCards.length; i++) {
        playerCards[i].position.x = 0.5 * app.renderer.width + (i - (playerCards.length -1) / 2) * 150;
        playerCards[i].position.y = 0.5 * app.renderer.height + 200;
    }
    
    app.stage.addChild(card);
}


function addDealerCard(card) {
    dealerCards.push(card);
    updateHandValuesTexts();
    for(let i = 0; i < dealerCards.length; i++) {
        dealerCards[i].position.x = 0.5 * app.renderer.width + (i - (dealerCards.length -1) / 2) * 150;
        dealerCards[i].position.y = 0.5 * app.renderer.height - 200;
    }
    
    app.stage.addChild(card);
}


function determineWinner() {
    updateHandValuesTexts();
    let playerVal = getHandValue(playerCards); 
    let dealerVal = getHandValue(dealerCards); 

    // hide ui buttons
    hitButton.visible = false;
    standButton.visible = false;
    doubleButton.visible = false;
    splitButton.visible = false;
    
    if(playerVal > 21) {
        statusText.text = "BUST";
    }
    else if(playerVal === dealerVal) {
        statusText.text = "PUSH";
        changeBalance(bet);
    }
    else if(playerVal < dealerVal && dealerVal <= 21) {
        statusText.text = "LOST";
    }
    else {
        statusText.text = "YOU WIN!";
        changeBalance(2 * bet);
    }

    changeBet(-bet);
    statusText.visible = true;
    
    // tmp, wait for click to reset game
    a = true;
}


// prepare for new hand
function reset() {
    waitForClickToReset = false;
    deck.shuffle();
    deck.currCardIndex = 0;
    for(let i = 0; i < playerCards.length; i++) {
        app.stage.removeChild(playerCards[i]);
    }
    for(let i = 0; i < dealerCards.length; i++) {
        app.stage.removeChild(dealerCards[i]);
    }
    playerCards = [];
    dealerCards = [];
    playerHandText.visible = false;
    dealerHandText.visible = false;

    if(balance === 0) {
        statusText.text = "NO MORE CASH\nGAME OVER"
        dealButton.visible = false;
        return;
    }

    statusText.text = "Place your bets...";
    statusText.visible = true;
    dealButton.visible = true;
    updateVisibleCoins();
}


function getHandValue(arr) {
    let val = 0;
    let numAces = 0;

    for(let i = 0; i < arr.length; i++) {
        if(arr[i].value == 11) {
            numAces++;
            val++;
        }
        else {
            val += arr[i].value;
        }
    }

    if(val <= 11 && numAces > 0) {
        val += 10;
    }

    return val;
}